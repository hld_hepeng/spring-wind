﻿![spring-wind](http://git.oschina.net/uploads/images/2016/0410/215321_0c4657f5_12260.png "SSM架构核心库")

SSM 架构核心库，带给您的只有惊喜！

[演示项目 + 文档说明 - 点击](http://git.oschina.net/juapk/SpringWind) **效果图往下拉！**

> 技术讨论 QQ 群 235079513 

------------------------------------------------
《春风》

春风如贵客，一到便繁华。

来扫千山雪，归留万国花。

------------------------------------------------

演示界面
=======

![登录效果](http://git.oschina.net/uploads/images/2016/0415/233200_ee76b11a_12260.png "登录效果")

![后台发送邮件测试](http://git.oschina.net/uploads/images/2016/0417/195044_19dcb437_12260.png "后台发送邮件测试")

![后台界面效果](http://git.oschina.net/uploads/images/2016/0415/233226_7713bf12_12260.png "后台界面效果")

![锁定效果](http://git.oschina.net/uploads/images/2016/0415/233245_dc44f2f9_12260.png "锁定效果")



Features
=======

1、欢迎提出更好的意见，帮助完善 Spring-Wind 

Copyright
====================
Apache License, Version 2.0